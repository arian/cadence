package nl.tudelft.cadence;

import java.util.Calendar;

import nl.tudelft.cadence.data.DataStack;
import nl.tudelft.cadence.data.FrequencyAnalysis;
import nl.tudelft.cadence.data.FrequencyAnalyzer;
import nl.tudelft.cadence.data.TimeInterpolation;
import nl.tudelft.cadence.data.Unbox;
import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.jjoe64.graphview.GraphView.GraphViewData;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.LineGraphView;

public class MainActivity extends Activity implements SensorEventListener {

	private boolean isRunning = false;

	/**
	 * Objects in the main layout
	 */
	private TextView statusText;
	private ToggleButton runningButton;
	private TextView frequencyText;
	private TextView powerText;
	private LinearLayout graphLayout;
	private LineGraphView graphView;
	private GraphViewSeries graphSeries;

	/**
	 * Sensor and SensorManager objects
	 */
	private SensorManager mSensorManager;
	private Sensor mAccelerometer;

	/**
	 * Stack that saves the acceleration values DATA_SIZE must be a power of 2.
	 */
	private final int DATA_SIZE = 256;
	private final DataStack<Double> data = new DataStack<Double>(DATA_SIZE);
	private final DataStack<Long> time = new DataStack<Long>(DATA_SIZE);

	/**
	 * Sampling Frequency / delay
	 */
	private final int SAMPLING_DELAY = SensorManager.SENSOR_DELAY_GAME;
	private double sf;

	/**
	 * Delay between each analyze task to run in ms.
	 */
	private final int ANALYZE_DELAY = 100;
	private final int DRAW_GRAPH_DELAY = 1000;

	/**
	 * Timers
	 */
	private final Handler analyzeHandler = new Handler();
	private final Handler drawGraphHandler = new Handler();

	/**
	 * Minimal power for the frequency to be added to the graph
	 */
	private final double MIN_POWER = 10;

	/**
	 * Frequencies of the last time in seconds. Frequencies stack holds both the
	 * frequency and the time.
	 */
	private final int SAVE_FREQUENCIES_FOR_TIME = 60;
	private final DataStack<Double> frequencies = new DataStack<Double>(
			(int) (2 * (SAVE_FREQUENCIES_FOR_TIME * 1e3 / ANALYZE_DELAY)));

	/**
	 * Initialize all fields and set event listeners
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// layout items
		statusText = (TextView) findViewById(R.id.status);
		runningButton = (ToggleButton) findViewById(R.id.running);
		frequencyText = (TextView) findViewById(R.id.frequency);
		powerText = (TextView) findViewById(R.id.power);
		graphLayout = (LinearLayout) findViewById(R.id.graph);

		runningButton
				.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							MainActivity.this.startMeasuring();
						} else {
							MainActivity.this.stopMeasuring();
						}
					}
				});

		// create graph
		graphView = new LineGraphView(this, "Cadence");
		graphLayout.addView(graphView);

		// Get Accelerometer Sensor
		mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		mAccelerometer = mSensorManager
				.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

		// Translate sampling frequency
		switch (SAMPLING_DELAY) {
		case SensorManager.SENSOR_DELAY_FASTEST:
			sf = 100;
			break;
		case SensorManager.SENSOR_DELAY_GAME:
			sf = 50;
			break;
		case SensorManager.SENSOR_DELAY_UI:
			sf = 30;
			break;
		case SensorManager.SENSOR_DELAY_NORMAL:
			sf = 5;
		}
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	/**
	 * Start measuring by activating the accelerometer and timer.
	 */
	private void startMeasuring() {
		if (isRunning)
			return;
		isRunning = true;
		mSensorManager.registerListener(this, mAccelerometer, SAMPLING_DELAY);
		runAnalysisTask();
		runDrawGraphTask();
		statusText.setText(R.string.running);
	}

	/**
	 * stop measuring by stopping the accelerometer and timer.
	 */
	private void stopMeasuring() {
		if (!isRunning)
			return;
		isRunning = false;
		mSensorManager.unregisterListener(this);
		analyzeHandler.removeCallbacks(mRunAnalysisTask);
		drawGraphHandler.removeCallbacks(mRunDrawGraphTask);
		statusText.setText(R.string.stopped);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	@Override
	public void onSensorChanged(SensorEvent event) {

		double acceleration = event.values[0] * event.values[0]
				+ event.values[1] * event.values[1] + event.values[2]
				* event.values[2];

		data.push(acceleration);
		time.push(event.timestamp);
	}

	/**
	 * Run next analysis task with a delay.
	 */
	private void runAnalysisTask(int delay) {
		analyzeHandler.removeCallbacks(mRunAnalysisTask);
		analyzeHandler.postDelayed(mRunAnalysisTask, delay);
	}

	private void runAnalysisTask() {
		runAnalysisTask(ANALYZE_DELAY);
	}

	private final Runnable mRunAnalysisTask = new Runnable() {

		@Override
		public void run() {

			// Convert to long[] and double[]
			double[] a = Unbox.fromDouble(data.toArray());
			long[] t = Unbox.fromLong(time.toArray());

			// Sample and interpolate data with constant interval
			double[] d = TimeInterpolation.sample(t, a, (long) (1e9 / sf));
			d = TimeInterpolation.getLastPowerOf2(d);

			// Analyze data
			FrequencyAnalysis result = FrequencyAnalyzer.analyze(d, sf);
			// Get frequency from data
			double freq = result.getMaxFrequency(0.5, sf / 2);
			double power = result.getTotalPower();

			frequencyText.setText(String.format("%.2f RPM", freq * 60));
			powerText.setText(String.format("%.2f", power));

			if (power > MIN_POWER) {
				// save frequency
				frequencies.push(freq);
				// and the time
				Calendar c = Calendar.getInstance();
				double seconds = c.getTimeInMillis() / 1000;
				frequencies.push(seconds);
			}

			// Start next task
			if (isRunning) {
				runAnalysisTask();
			}
		}

	};

	/**
	 * Run next draw graph task with a delay.
	 */
	private void runDrawGraphTask(int delay) {
		drawGraphHandler.removeCallbacks(mRunDrawGraphTask);
		drawGraphHandler.postDelayed(mRunDrawGraphTask, delay);
	}

	private void runDrawGraphTask() {
		runDrawGraphTask(DRAW_GRAPH_DELAY);
	}

	private final Runnable mRunDrawGraphTask = new Runnable() {

		private Double startTime;

		@Override
		public void run() {

			int n = frequencies.getSize();

			if (n >= 2) {

				GraphViewData[] data = new GraphViewData[n / 2];

				if (startTime == null) {
					startTime = frequencies.get(1);
				}

				for (int i = 0, j = 0; i < n; i += 2, j++) {
					data[j] = new GraphViewData(frequencies.get(i + 1)
							- startTime, frequencies.get(i));
				}

				if (graphSeries != null) {
					graphSeries.resetData(data);
				} else {
					graphSeries = new GraphViewSeries(data);
					graphView.addSeries(graphSeries);
				}

			}

			// Start next task
			if (isRunning) {
				runDrawGraphTask();
			}

		}

	};

}
