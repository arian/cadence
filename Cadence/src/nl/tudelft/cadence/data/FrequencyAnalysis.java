package nl.tudelft.cadence.data;

public class FrequencyAnalysis {

	private double[] power;
	private double[] frequencies;
	private double samplingFrequency;

	public double[] getPower() {
		return power;
	}

	public void setPower(double[] power) {
		this.power = power;
	}

	public double[] getFrequencies() {
		return frequencies;
	}

	public void setFrequencies(double[] frequencies) {
		this.frequencies = frequencies;
	}

	public void setSamplingFrequency(double samplingFrequency) {
		this.samplingFrequency = samplingFrequency;
	}

	public double getSamplingFrequency() {
		return this.samplingFrequency;
	}

	public double getMaxFrequency() {
		return getMaxFrequency(0, samplingFrequency / 2);
	}

	public double getMaxFrequency(double min, double max) {
		double p = 0;
		double f = 0;
		if (frequencies == null) {
			return f;
		}
		for (int i = 0; i < frequencies.length; i++) {
			if (frequencies[i] < min)
				continue;
			if (frequencies[i] > max)
				break;
			if (power[i] > p) {
				p = power[i];
				f = frequencies[i];
			}
		}
		return f;
	}

	public double getTotalPower() {
		double p = 0;
		if (power == null) {
			return 0;
		}
		for (int i = 0; i < power.length; i++) {
			p += power[i];
		}
		return p / power.length;
	}

}
