package nl.tudelft.cadence.data;

public class Range {

	public static Range from(int min) {
		return new Range(min);
	}

	private final int min;
	private int max;
	private int step = 1;
	private int pointer;

	public Range(int min) {
		this.min = pointer = min;
	}

	public Range(int min, int max) {
		this.min = pointer = min;
		this.max = max;
	}

	public Range to(int max) {
		this.max = max;
		return this;
	}

	public Range setStep(int n) {
		step = n;
		return this;
	}

	public int step() {
		return this.pointer += step;
	}

	public int[] asArray() {
		int n = max < min ? 1 : (((max - min) / step) + 1);
		int[] res = new int[n];
		res[0] = min;
		for (int i = 1; i < n; i++) {
			res[i] = res[i - 1] + step;
		}
		return res;
	}
}
