package nl.tudelft.cadence.data;

import nl.tudelft.cadence.fft.FFT;

public class FrequencyAnalyzer {

	/**
	 * 
	 * @param data
	 * @param sf
	 *            Sampling frequency
	 * @return
	 */
	public static FrequencyAnalysis analyze(double[] data, double sf) {
		FrequencyAnalysis result = new FrequencyAnalysis();
		result.setSamplingFrequency(sf);

		int m = data.length;

		double[] inputImag = new double[m];
		double[] y = FFT.fft(data, inputImag, true);

		if (y != null) {

			double[] p = calculatePower(y, m);
			double[] f = frequencies(sf, m);
			result.setPower(p);
			result.setFrequencies(f);

		}

		return result;
	}

	/**
	 * Create array of frequencies of the signal.
	 * 
	 * @param sf
	 *            Sampling Frequency
	 * @param m
	 *            Length of the data
	 * @return array with length m with frequencies
	 */
	private static double[] frequencies(double sf, int m) {
		double[] f = new double[m];
		for (int i = 0; i < m; i++) {
			f[i] = i * sf / m;
		}
		return f;
	}

	/**
	 * Calculate the power of the signal.
	 * 
	 * @param y
	 *            fft result
	 * @param m
	 *            length of data
	 * @return array of length m with the power of the signal
	 */
	private static double[] calculatePower(double[] y, int m) {
		double[] p = new double[m];

		for (int i = 0, j = 0; i < y.length; i += 2, j++) {
			p[j] = y[i] * y[i] + y[i + 1] * y[i + 1];
		}

		return p;
	}

}
