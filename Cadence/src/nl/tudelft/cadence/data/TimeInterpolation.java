package nl.tudelft.cadence.data;

public class TimeInterpolation {

	/**
	 * Sample data for with a certain frequency
	 * 
	 * @param x
	 *            the time vector
	 * @param y
	 *            the data vector
	 * @param dt
	 *            sampling time (1 / sampling frequency)
	 * @return sampled data, with dt between each entry
	 */
	@Deprecated
	public static double[] sample(double[] x, double[] y, double dt) {
		assert x.length == y.length : "x and y should have the same length";

		// nothing to do
		if (x.length < 2) {
			return new double[] {};
		}

		double min = x[0];
		double max = x[x.length - 1];

		// number of samples we can get from the lists
		int n = (int) Math.floor((max - min) / dt);

		double[] points;

		// max and min are the same.
		if (n == 0) {
			points = new double[x.length];
			for (int i = 0; i < x.length; i++) {
				points[i] = min;
			}
			return points;
		}

		points = new double[n];

		for (int i = 0, j = 0; i < n; i++) {
			double t = min + i * dt;
			while (x[j] <= t) {
				j++;
			}
			points[i] = y[j] - (y[j] - y[j - 1]) * (x[j] - t)
					/ (x[j] - x[j - 1]);
		}

		return points;
	}

	/**
	 * Sample an array with long x
	 * 
	 * @param t
	 *            time vector
	 * @param y
	 *            data matrix
	 * @param dt
	 *            sampling time (1 / sampling frequency)
	 * @return sampled data, with dt between each entry.
	 */
	public static double[] sample(long[] t, double[] y, long dt) {
		assert t.length == y.length : "x and y should have the same length";

		// nothing to do
		if (t.length < 2) {
			return new double[] {};
		}

		long min = t[0];
		long max = t[t.length - 1];

		// number of samples we can get from the lists
		int n = (int) Math.floor((max - min) / dt);

		double[] points;

		// max and min are the same.
		if (n == 0) {
			points = new double[t.length];
			for (int i = 0; i < t.length; i++) {
				points[i] = min;
			}
			return points;
		}

		points = new double[n];

		for (int i = 0, j = 0; i < n; i++) {
			long time = min + i * dt;
			while (t[j] <= time) {
				j++;
			}
			points[i] = y[j] - (y[j] - y[j - 1]) * (t[j] - time)
					/ (t[j] - t[j - 1]);
		}

		return points;
	}

	/**
	 * 
	 * @return P such that 2^P < n
	 */
	private static int getPowerOf2(int n) {
		return (int) Math.floor(Math.log(n) / Math.log(2));
	}

	/**
	 * 
	 * @param x
	 * @return the last 2^n items of the array, where 2^n < x.length.
	 */
	public static double[] getLastPowerOf2(double[] x) {
		int n = x.length;
		int p = (int) Math.pow(2, getPowerOf2(n));
		double[] res = new double[p];
		for (int i = 0; i < p; i++) {
			res[i] = x[i + n - p];
		}
		return res;
	}
}
