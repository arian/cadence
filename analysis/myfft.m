
m = 64;
data = zeros(m, 1);

df = 2;
sf = 5;

for i = 1:m
    data(i) = sin((i - 1) * (2 * pi) * df / sf);
end

plot(data);

%%

y = fft(data, m);       % DFT of signal
f = (0:m-1)*(sf/m);       % Frequency range
p = y.*conj(y);         % Power of the DFT

plot(f, p);