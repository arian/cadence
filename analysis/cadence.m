
%% import data

[time, x, y, z] = importfile('./cadence.csv', 100, inf);

%% plot data

hold on
plot(time, x, 'r');
plot(time, y, 'b');
plot(time, z, 'g');

%% fourier transformation

n = length(time);
totalTime = (max(time) - min(time));

dt = totalTime / n;

xi = zeros(n, 1);
t = time(1);
t0 = t;

index = 1;
prevX = x(1);

for i = 1:n
    tnow = t0 + i * dt;
    while time(index + 1) < tnow
        index = index + 1;
        t = time(index);
    end
    if (i == n)
        xi(i) = x(i);
    else
        xi(i) = (tnow - t) / dt * (x(index) - prevX) + prevX;
    end
end
%%


figure
hold on
plot(xi, 'r');

xindices = zeros(1, n);
for i = 1:n
    xindices(i) = i;
end

plot(xindices, x, 'b');

%% fft for 128 samples

samples = 128;
start = floor(length(time) / 2);

xj = x(start:(start + samples));

m = length(xj);          % Window length
y = fft(xj,m);           % DFT
f = (0:m-1)*(fs/m);     % Frequency range
power = y.*conj(y)/m;   % Power of the DFT

fstart = find(f > 0.3, 1);
fend = find(f < 3, 1, 'last');

f = f(fstart:fend);
power = power(fstart:fend);


plot(f, power)
xlabel('frequency Hz');
ylabel('power');

%% fft

len = length(time);

fs = 1e9 / dt;
st = 10;
samples = 128;

frequencies = zeros(floor(len / samples) - 1, 1)

for i = 1:length(frequencies)

    xj = x((i*samples):((i+1)*samples - 1));

    m = length(xj)          % Window length
    y = fft(xj,m);           % DFT
    f = (0:m-1)*(fs/m);     % Frequency range
    power = y.*conj(y)/m;   % Power of the DFT

    fstart = find(f > 0.2, 1)
    fend = find(f < 3, 1, 'last');
    
    f = f(fstart:fend);
    power = power(fstart:fend);

    [a, maxIndex] = max(power);
    maxFrequency = f(maxIndex);
    frequencies(i) = maxFrequency;

end

plot(frequencies)
