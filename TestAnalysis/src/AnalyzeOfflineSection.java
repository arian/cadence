import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JFrame;

import nl.tudelft.cadence.data.DataStack;
import nl.tudelft.cadence.data.FrequencyAnalysis;
import nl.tudelft.cadence.data.FrequencyAnalyzer;
import nl.tudelft.cadence.data.TimeInterpolation;
import nl.tudelft.cadence.data.Unbox;

import org.math.plot.Plot2DPanel;

public class AnalyzeOfflineSection {

	private final int DATA_SIZE = 128;
	private final DataStack<Double> data = new DataStack<Double>(DATA_SIZE);
	private final DataStack<Long> time = new DataStack<Long>(DATA_SIZE);

	public AnalyzeOfflineSection() {

		int count = 0;

		try {
			File file = new File("../analysis/cadence.csv");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			String line;
			while ((line = br.readLine()) != null) {
				readData(line);
				if (count < 128) {
					continue;
				} else if (count >= 256) {
					break;
				}
				count++;
			}

			analyzeData();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void readData(String line) {
		Scanner sc = new Scanner(line);
		sc.useDelimiter(",");
		long t = sc.nextLong();
		double x = sc.nextDouble();
		double y = sc.nextDouble();
		double z = sc.nextDouble();
		time.push(t);
		data.push(x * x + y * y + z * z);
	}

	private void analyzeData() {
		// Sampling frequency
		double sf = 5;

		double[] a = Unbox.fromDouble(data.toArray());
		long[] t = Unbox.fromLong(time.toArray());

		System.out.println("" + a.length);

		// Sample and interpolate data with constant interval
		double[] d = TimeInterpolation.sample(t, a, (long) (1e9 / sf));
		d = TimeInterpolation.getLastPowerOf2(d);

		// Analyze data
		FrequencyAnalysis result = FrequencyAnalyzer.analyze(d, sf);

		plotFrequencies(result);

	}

	private void plotFrequencies(FrequencyAnalysis result) {
		// create your PlotPanel (you can use it as a JPanel)
		Plot2DPanel plot = new Plot2DPanel();

		// define the legend position
		plot.addLegend("SOUTH");

		double[] x = result.getFrequencies();
		double[] y = result.getPower();

		// add a line plot to the PlotPanel
		plot.addLinePlot("frequencies", x, y);

		// put the PlotPanel in a JFrame like a JPanel
		JFrame frame = new JFrame("a plot panel");
		frame.setSize(600, 600);
		frame.setContentPane(plot);
		frame.setVisible(true);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new AnalyzeOfflineSection();
	}
}
