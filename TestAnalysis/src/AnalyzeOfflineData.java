import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JFrame;

import nl.tudelft.cadence.data.DataStack;
import nl.tudelft.cadence.data.FrequencyAnalysis;
import nl.tudelft.cadence.data.FrequencyAnalyzer;
import nl.tudelft.cadence.data.Range;
import nl.tudelft.cadence.data.TimeInterpolation;
import nl.tudelft.cadence.data.Unbox;

import org.math.plot.Plot2DPanel;

public class AnalyzeOfflineData {

	private final int DATA_SIZE = 64;
	private final DataStack<Double> data = new DataStack<Double>(DATA_SIZE);
	private final DataStack<Long> time = new DataStack<Long>(DATA_SIZE);
	private final ArrayList<Double> freqs = new ArrayList<Double>();

	public AnalyzeOfflineData() {

		int count = 0;

		try {
			File file = new File("../analysis/cadence.csv");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);

			String line;
			while ((line = br.readLine()) != null) {
				readData(line);
				if (count++ % 20 == 0) {
					analyzeData();
				}
			}

			plotFrequencies();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void readData(String line) {
		Scanner sc = new Scanner(line);
		sc.useDelimiter(",");
		long t = sc.nextLong();
		double x = sc.nextDouble();
		double y = sc.nextDouble();
		double z = sc.nextDouble();
		time.push(t);
		data.push(x * x + y * y + z * z);
	}

	private void analyzeData() {
		// Sampling frequency
		double sf = 5;

		// Convert to long[] and double[]
		ArrayList<Double> accel = data.toArray();
		ArrayList<Long> tim = time.toArray();

		double[] a = new double[accel.size()];
		long[] t = new long[accel.size()];

		for (int i = 0; i < accel.size(); i++) {
			a[i] = accel.get(i);
			t[i] = tim.get(i);
		}

		// Sample and interpolate data with constant interval
		double[] d = TimeInterpolation.sample(t, a, (long) (1e9 / sf));
		d = TimeInterpolation.getLastPowerOf2(d);

		// Analyze data
		FrequencyAnalysis result = FrequencyAnalyzer.analyze(d, sf);
		// Get frequency from data
		double freq = result.getMaxFrequency(0.5, sf / 2);

		System.out.println(freq);

		freqs.add(freq);
	}

	private void plotFrequencies() {
		// create your PlotPanel (you can use it as a JPanel)
		Plot2DPanel plot = new Plot2DPanel();

		// define the legend position
		plot.addLegend("SOUTH");

		double[] x = Unbox.intsToDoubles(Range.from(0).to(freqs.size() - 1)
				.asArray());
		double[] y = Unbox.fromDouble(freqs);

		// add a line plot to the PlotPanel
		plot.addLinePlot("frequencies", x, y);

		// put the PlotPanel in a JFrame like a JPanel
		JFrame frame = new JFrame("a plot panel");
		frame.setSize(600, 600);
		frame.setContentPane(plot);
		frame.setVisible(true);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new AnalyzeOfflineData();
	}
}
