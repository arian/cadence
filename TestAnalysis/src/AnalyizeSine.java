import javax.swing.JFrame;

import nl.tudelft.cadence.data.FrequencyAnalysis;
import nl.tudelft.cadence.data.FrequencyAnalyzer;

import org.math.plot.Plot2DPanel;

public class AnalyizeSine {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int m = 32;
		double[] data = new double[m];
		double f1 = 0.75;
		double f2 = 1;
		double sf = 5; // sampling frequency
		for (int i = 0; i < m; i++) {
			data[i] = Math.sin(i * 2 * Math.PI * f1 / sf)
					+ Math.sin(i * 2 * Math.PI * f2 / sf) * 0.5;
		}
		FrequencyAnalysis result = FrequencyAnalyzer.analyze(data, sf);

		System.out.println("Frequency: " + result.getMaxFrequency());

		// create your PlotPanel (you can use it as a JPanel)
		Plot2DPanel plot = new Plot2DPanel();

		// define the legend position
		plot.addLegend("SOUTH");

		// add a line plot to the PlotPanel
		plot.addLinePlot("my plot", result.getFrequencies(), result.getPower());

		// put the PlotPanel in a JFrame like a JPanel
		JFrame frame = new JFrame("a plot panel");
		frame.setSize(600, 600);
		frame.setContentPane(plot);
		frame.setVisible(true);
	}

}
