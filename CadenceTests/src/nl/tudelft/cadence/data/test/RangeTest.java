package nl.tudelft.cadence.data.test;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import nl.tudelft.cadence.data.Range;

import org.junit.Test;

public class RangeTest {

	@Test
	public void testSimpleRange() {
		int[] res = Range.from(3).to(10).asArray();
		assertTrue(Arrays.equals(new int[] { 3, 4, 5, 6, 7, 8, 9, 10 }, res));
	}

	@Test
	public void testRangeWithoutMax() {
		int[] res = Range.from(3).asArray();
		assertTrue(Arrays.equals(new int[] { 3 }, res));
	}

	@Test
	public void testRangeWithStep() {
		int[] res = Range.from(3).to(10).setStep(3).asArray();
		assertTrue(Arrays.equals(new int[] { 3, 6, 9 }, res));
	}

}
