package nl.tudelft.cadence.data.test;

import static org.junit.Assert.assertEquals;
import nl.tudelft.cadence.data.FrequencyAnalysis;
import nl.tudelft.cadence.data.FrequencyAnalyzer;

import org.junit.Test;

public class FrequencyAnalyzerTest {

	@Test
	public void testSimple() {
		int m = 64;
		double[] data = new double[m];
		double f = 2;
		double sf = 5; // sampling frequency
		for (int i = 0; i < m; i++) {
			data[i] = Math.sin(i * 2 * Math.PI * f / sf);
		}

		FrequencyAnalysis result = FrequencyAnalyzer.analyze(data, sf);
		double freq = result.getMaxFrequency();

		assertEquals(f, freq, 0.1);
	}

	@Test
	public void testWithTwoFrequenceis() {
		int m = 32;
		double[] data = new double[m];
		double f1 = 2;
		double f2 = 1;
		double sf = 5; // sampling frequency
		for (int i = 0; i < m; i++) {
			data[i] = Math.sin(i * 2 * Math.PI * f1 / sf)
					+ Math.sin(i * 2 * Math.PI * f2 / sf) * 0.5;
		}

		FrequencyAnalysis result = FrequencyAnalyzer.analyze(data, sf);
		double freq = result.getMaxFrequency();

		assertEquals(f1, freq, 0.1);
	}

	@Test
	public void testWithZeros() {
		int m = 4;
		double[] data = new double[m];
		FrequencyAnalysis result = FrequencyAnalyzer.analyze(data, 5);
		double freq = result.getMaxFrequency();
		assertEquals(0, freq, 1e-6);
	}

	@Test
	public void testWithNull() {
		double[] data = new double[0];
		FrequencyAnalysis result = FrequencyAnalyzer.analyze(data, 5);
		double freq = result.getMaxFrequency();
		assertEquals(0, freq, 1e-6);
	}

}
