package nl.tudelft.cadence.data.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import nl.tudelft.cadence.data.TimeInterpolation;

import org.junit.Test;

public class TimeInterpolationTest {

	/**
	 * Test TimeInterpolation.sample with 1d data vector with a dt > 1, so that
	 * results in more data points than the original.
	 */
	@Test
	public void testTimeInterpolationHighFrequency() {
		double[] res = TimeInterpolation.sample(new double[] { 0, 1, 2, 4 },
				new double[] { 1, 3, 4, 2 }, 0.5);

		assertEquals(8, res.length);
		assertTrue(Arrays.equals(res, new double[] { 1, 2, 3, 3.5, 4, 3.5, 3,
				2.5 }));
	}

	@Test
	public void testWithZeros() {
		double[] res = TimeInterpolation.sample(new double[] { 0, 0, 0, 0 },
				new double[] { 0, 0, 0, 0 }, 0.2);
		assertEquals(4, res.length);
		assertTrue(Arrays.equals(res, new double[] { 0, 0, 0, 0 }));
	}

	/**
	 * Test TimeInterpolation.sample with 1d data with a high dt.
	 */
	@Test
	public void testTimeInterpolationLowFrequency() {
		double[] res = TimeInterpolation.sample(new double[] { 0, 1, 2, 4 },
				new double[] { 1, 3, 4, 2 }, 2);

		assertEquals(2, res.length);
		assertTrue(Arrays.equals(res, new double[] { 1, 4 }));
	}

	@Test
	public void testTimeInterpolationLongs() {
		double[] res = TimeInterpolation.sample(new long[] { 0, 1, 2, 4 },
				new double[] { 1, 3, 4, 2 }, 2);

		assertEquals(2, res.length);
		assertTrue(Arrays.equals(res, new double[] { 1, 4 }));
	}

	@Test
	public void testGetLastPowerOf2() {
		double[] res = TimeInterpolation.getLastPowerOf2(new double[] { 1, 2,
				3, 4, 5, 6, 7, 8, 9, 10 });

		assertEquals(8, res.length);
		assertTrue(Arrays.equals(res, new double[] { 3, 4, 5, 6, 7, 8, 9, 10 }));

	}
}
